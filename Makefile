analyze-all: 
	ghdl -a vhdl/*
	ghdl -a testbench/*

run: analyze-all
	ghdl -r $(entity)
